![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de Semantica HTML5 y CSS3
==================

## Consejos practicos
- Descargar la ultima versión de la plantilla establecida en SMDigital para el inicio de un proyecto, desde el repositorio de [Bitbucket][BK]
- IDE por preferencia [Sublime Text 2/3][ST]
- NO maquetar sitios web en Tablas, solo se deben utilizar en los siguientes casos:
	- Envios de Emails
	- Tabulación de Contenidos ( Listados, Graficos, reportes )
- Por temas de SEO y una buena web con semantica debemos utilizar las principales etiquetas de HTML5 `header, nav, section, article, figure, figcaption, aside, footer`, para más información consultar en la [Tabla perdiodica de HTML5][TP]

## HTML

### HTML sintaxis
- Siempre utilizar `TAB`
- Siempre utilizar comillas dobles `""`
- Nunca incluir el cierre `/` de en una etiqueta unica
- Nunca incluir la etiqueta `<section>` dentro de un `<article>`
- Nunca utilizar dos veces las misma propiedad, ejemplo `<input class="wrapper" class="container" type="text">` forma correcta `<input class="wrapper container" type="text">`


```html
// Sintaxis Incorrecta
<!DOCTYPE html>
<html>
<head>
<title>Page title</title>
</head>
<body>
<img src='images/company-logo.png' alt='Company' />
<h1 class='hello-world'>Hello, world!</h1>
<article>
<section>Loremp</section>
</article>
</body>
</html>

// Sintaxis Correcta!
<!DOCTYPE html>
<html>
	<head>
		<title>Page title</title>
	</head>
	<body>
		<img src="images/company-logo.png" alt="Company">
		<h1 class="hello-world">Hello, world!</h1>
		<section>
			<article>
				<div>Loremp</div>
				<p>Loremp</p>
			</article>
		</section>
	</body>
</html>
```
### HTML5 doctype
En el comienzo de cada página DEBE de ir el doctype

```html
<!DOCTYPE html>
```

### Orden en los Atributos
Los artributos de las etiquetas HTML DEBEN entrar en un orden particular, para la facil lectura del código

- class
- id
- data-*
- for|src|href|alt|title

```html
<a class="inline" id="logo-smdigital" data-modal="" href="//www.smdigital.com.co" title="SMDigital"></a>
```

### Nombramiento de clases y id's
DEBE declararse en minusculas y en ingles, si hay espacios debe ser reemplazado por guines `-`. No utilizar prefijos, sino nombre completos y claros

```html
<nav class="inline" id="main-menu"></nav>
```

### Etiquetas que NO se deben Utilizar
- hgroup
- center
- b
- marquee

## CSS3

### CSS3 sintaxis

- Siempre utilizar `TAB`
- Si los atributos de un selector son solo 2 dejarlo en una sola linea, ejemplo `#main-menu { margin: 0; padding: 0;  }`
- Siempre mantener el order de las propiedades de un atributo, si no recordamos el orden podemos consultar en [W3Schools][W3S]
- Incluir comillas simples `'` en los atributos `url()`
- Incluir un espacio antes de la apertura de los selectores `#main-menu {`
- Incluir un espacio despues de cada propiedad `display: block;`
- Ponerle fin a las declaraciones con un punto y coma `;`
- Minusculas todos los valores hexadecimales, ejemplo `#fff`
- Citar el valor de los atributos de los selectores, ejemplo `input[type="text"]`
- Evitar especificar unidades para los valores cero, ejemplo `margin: 0` en lugar de `margin: 0px;`
- Al agrupar selectores, mantenga los selectores individuales e una sola linea
- Siempre tener los selectores en el siguiente orden y alfabeticamente:
	1. etiquetas
	2. id
	3. class
- Siempre tener los atributos de los selectores ordenados alfabeticamente

```html
	// Sintaxis incorrecta
	#main-menu, footer #sub-menu, header{
	outline:none;
	display:inline-block;
	color:#FFF;
	padding:0px 0x 10px 50px
	margin:0px auto;
	background: center top url(../img/bg-body.png) #FFF no-repeat;
	}

	// Sintaxis correcta
	footer,
	header,
	#main-menu,
	#sub-menu {
		background: #fff url('../img/bg-body.png') no-repeat center top;
		color: #fff;
		display: inline-block;
		margin: auto;
		padding: 0 0 10px 50px;
		outline: none;
	}
```

[ST]: http://sublimetext.com
[BK]: https://bitbucket.org/smdigital/estructura-de-carpetas/
[W3S]: http://www.w3schools.com
[TP]: http://joshduck.com/periodic-table.html
