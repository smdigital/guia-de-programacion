![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de programación PHP
=========================== 

## Etiquetas PHP

El código PHP debe utilizar las etiquetas `<?php ?>` o las etiquetas cortas para imprimir información `<?= ?>`. [^1]

## Codificación de caracteres

El código PHP DEBE utilizar codificación UTF-8.

## Clases
  
Los nombres de las clases deben declararse con notación **StudlyCaps**. [^2]

El código escrito para PHP 5.3 o superior s hacer un uso formal de los espacios de nombres.

Por ejemplo:

```php
<?php
// PHP 5.3 o superior:
namespace Proveedor\Modelo;
class Foo
{
}
```

El código escrito para PHP 5.2.x o inferior debería emplear una convención de **pseudo-espacios**.


```php
<?php
// PHP 5.2.x o inferior:
class Modelo_Foo
{
}
```

## Constantes de Clases, Propiedades y Métodos

El término "clases" hace referencia a todas las clases, interfaces y traits.

## Constantes

Las constantes de las clases deben declararse siempre en mayúsculas y separadas por guiones bajos. Por ejemplo:

```php
<?php
class Foo
{
    const VERSION = '1.0';
    const FECHA_DE_APROBACION = '2012-06-01';
}
```

## Métodos

Los nombres de los métodos deben declararse en notación **camelCase()**. [^3]

## General 

- El código debe ser tabulado, no con espacios.

- Las llaves de apertura de las clases deben ir en la línea siguiente, y las llaves de cierre deben ir en la línea siguiente al cuerpo de la clase.

- Las llaves de apertura de los métodos deben ir en la línea siguiente, y las llaves de cierre deben que ir en la línea siguiente al cuerpo del método.

- Las llaves de apertura de las estructuras de control deben estar en la misma línea, y las de cierre deben ir en la línea siguiente al cuerpo.


### Ejemplo :
```php
<?php
class Foo extends Bar implements FooInterfaz
{
    public function functionExample($a, $b = null)
    {
        if ($a === $b) {
            bar();
        } elseif ($a > $b) {
            $foo->bar($arg1);
        } else {
            BazClase::bar($arg2, $arg3);
        }
    }

    final public static function bar()
    {
        // cuerpo del método
    }
}
```

## Ficheros

- Todos los ficheros PHP deben terminar con una línea en blanco.
- La etiqueta de cierre `?>` debe ser omitida en los ficheros que sólo contengan código PHP.

### Palabras clave y `true`/`false`/`null`.

Las [Palabras clave][Palabras clave] de PHP deben estar en minúsculas.

Las constantes de PHP `true`, `false` y `null` deben estar en minúsculas.

### `if`, `elseif`, `else`

Una estructura `if` tendrá el siguiente aspecto. Fíjese en el lugar de los paréntesis, los espacios y las llaves; y que `else` y `elseif` están en la misma línea que las llaves de cierre del cuerpo anterior.

```php
<?php
if ($expr1) {
    // if cuerpo
} elseif ($expr2) {
    // elseif cuerpo
} else {
    // else cuerpo;
}
```

La palabra clave `elseif` debería ser usada en lugar de `else if` de forma que todas las palabras clave de la estructura estén compuestas por palabras de un solo término.


### `switch`, `case`

Una estructura `switch` tendrá el siguiente aspecto. Fíjese en el lugar donde están los paréntesis, los espacios y las llaves. La palabra clave `case` debe estar indentada una vez respecto al `switch` y la palabra clave `break` o cualquier otra palabra clave de finalización debe estar indentadas al mismo nivel que el cuerpo del `case`. debe haber un comentario como // no break cuando hay case en cascada no vacío.

```php
<?php
switch ($expr) {
    case 0:
        echo 'Primer case con break';
        break;
    case 1:
        echo 'Segundo case sin break en cascada';
        // no break
    case 2:
    case 3:
    case 4:
        echo 'Tercer case; con return en vez de break';
        return;
    default:
        echo 'Case por defecto';
        break;
}
```

### `while`, `do while`

Una instrucción `while` tendrá el siguiente aspecto. Fíjese en el lugar donde están los paréntesis, los espacios y las llaves.

```php
<?php
while ($expr) {
    // cuerpo de la estructura
}
```

Igualmente, una sentencia `do while` tendrá el siguiente aspecto. Fíjese en el lugar donde están los paréntesis, los espacios y las llaves.

```php
<?php
do {
    // cuerpo de la estructura;
} while ($expr);
```

### `for`

Una sentencia `for` tendrá el siguiente aspecto. Fíjese en el lugar donde aparecen los paréntesis, los espacios y las llaves.

```php
<?php
for ($i = 0; $i < 10; $i++) {
    // cuerpo del for
}
```

### `foreach`

Un sentencia `foreach` tendrá el siguiente aspecto. Fíjese en el lugar donde aparecen los paréntesis, los espacios y las llaves.

```php
<?php
foreach ($iterable as $key => $value) {
    // cuerpo foreach
}
```

### `try`, `catch`

Un bloque `try catch` tendrá el siguiente aspecto. Fíjese en el lugar donde aparecen los paréntesis, los espacios y los llaves.

```php
<?php
try {
    // cuerpo del try
} catch (PrimerTipoDeExcepcion $e) {
    // cuerpo catch
} catch (OtroTipoDeExcepcion $e) {
    // cuerpo catch
}
```

## Nota

[^1] Versiones PHP <= 5 configurar el archivo php.ini y colocar el short_open_tag = On. [refencia][php.ini]

[^2] StudlyCaps, es una forma de notación de texto que sigue el patrón de palabras en minúscula sin espacios y con la primera letra de cada palabra en mayúscula.

[^3] camelCase, es una forma de notación de texto que sigue el patrón de palabras en minúscula sin espacios y con la primera letra de cada palabra en mayúsculas exceptuando la primera palabra.

[php.ini]: https://wiki.php.net/rfc/shortags
[Palabras clave]: http://php.net/manual/es/reserved.keywords.php