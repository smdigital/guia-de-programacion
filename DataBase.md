![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de Semantica Base de datos y Transact-SQL
===========================

## Consejos practicos
- El nombre de las bases de datos nos lo entregara el administrador de sistemas
- IDE por preferencia [MySQL Workbench][MW]
- Nombre de tablas, campos, procedimientos almacenados, triggers, deben ser nombrados en Ingles

## Semantica
### Tablas
- El nombramiento de las tablas debe ser de forma coherente con la información que almacenara
- Los nombres de las tablas deben ser en minúsculas
- Se debe utilizar un prefijo para las tablas, debe ser dos iniciales del proyecto asignado `sm_users`
- Al relacionar las tablas debe de quedar de la siguiente forma `sm_users_has_profile`

### Nombramiento de campos
- El nombramiento de los campos ser coherente con el campo que se almacenara
- Los nombres deben ser en minúsculas
- Si lleva espacios sera separado por guiones bajos `date_birth`
- No utilizar profijos como `int, dte, bool, txt, str` ya que no es una forma practica al momento de programar bajo Frameworks como Laravel, ya que este utiliza una estrutura igual a la que utilizaremos
- Utilizar campos enteros cuando se vaya a realizar operaciones matematicas
- Siempre tener una llave primaria ( Se deja a consideración del programador si utilizara un auto incrementable como llave primaria en cada tabla )
- El campo cedula ( utilizarlo como `VARCHAR(25)` ), campo email se debe colocar como UNICOS
- Recordar agregarle a los campos que seran mas utilizados para las buscadas la indexación `nombres, cedulas, email, fechas`

### Procedimiento almacenados
- El nombramiento de los procedimientos debe ser de forma coherente con la funcion que ejecutara
- Debemos utilizar prefijos al momento del nombramiento `usp`, si el nombre tiene espacios debe ser reemplazado por guiones bajos `_` ejemplo: `usp_insert_users`
- Utilizar los procedimientos almancenados para funciones que lleven muchas operaciones
- Tiene una buena recomedación al utilizar los procedimientos almacenados ya que hace las transacciones mas rapido que en PHP, pero no es bueno cuando tenemos que estar migrando de base de datos

## Transact-SQL

Tips para mejorar nuestras practicas en las transacciones SQL

- Utilizar poco el `*` ya que hace msa pesada y lenta la consulta
- Tener cuidado al hacer anidamientos de `SELECT` especificar muy bien los campos que utilizaremos para que sea rapida la consulta
- Si vamos hacer un `INSERT` y este sera con todos los campos de la tabla podemos evitar colocar los nombres, solamente los valores que vamos a insertar ( pero respetando el orden de los campos ) ejemplo: `INSERT INTO users VALUES ('pepito perez','pperez@smdigital.com.co','112844');`
- Para realizar una conversión de fecha ( eliminar las horas, minutos y segundos ) utilizar lo siguiente
```
SELECT DATE( NOW() );
/* Resultado */
2013/12/13
```
- Utilizar la propiedad DISTINC ya que nos ayuda a traer un unico resultado entre varias datos iguales, aparte que es mas rapido que un limit(1)



[MW]: http://www.mysql.com/products/workbench/