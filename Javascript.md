![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de programación Javascript
=========================== 

## Tipos

- Primitivos : Cuando se accede a un dato tipo primitivo se debe tratar directamente por su valor.
	
	+ `string`
	+ `number`
	+ `boolean`
	+ `null`
	+ `undefined`

```javascript
var foo = 1,
    bar = foo;

bar = 9;

console.log(foo, bar); // => 1, 9
```

- Complejos : Cuando se accede a un dato de tipo complejo se debe tratar con su valor a referencia.

	+ `object`
	+ `array`
	+ `function`

```javascript
var foo = [1, 2],
    bar = foo;

bar[0] = 9;

console.log(foo[0], bar[0]); // => 9, 9
```

## Objectos

- Utilice la sintaxis literal de la creacion del objecto.

```javascript
// Mal
var item = new Object();

// Bien
var item = {};
```

- No utlizar [palabras reservadas] como claves.

```javascript
// Mal
var superman = {
  class: 'superhero',
  default: { clark: 'kent' },
  private: true
};

// Bien
var superman = {
  klass: 'superhero',
  defaults: { clark: 'kent' },
  hidden: true
};
```

## Arrays

- Utilice la sintaxis literal de la creación de matrices

```javascript
// Mal
var items = new Array();

// Bien
var items = [];
```

- Para añadir elementos a la array utilizar Array#push

```javascript
var someStack = [];

// Mal
someStack[someStack.length] = 'abracadabra';

// Bien
someStack.push('abracadabra');
```

- Para copiar un array utilice Array#slice

```javascript
var len = items.length,
    itemsCopy = [],
    i;

// Mal
for (i = 0; i < len; i++) {
  itemsCopy[i] = items[i];
}

// Bien
itemsCopy = Array.prototype.slice.call(items);
```

## String

- Utilizar comillas simples ` ' ' ` para las cadenas

```javascript
// Bad 
var  name  =  "Bob Parr" ; 

// Bien 
var  name  =  'Bob Parr';

// Mal 
var  fullName  =  "Bob"  +  this.lastName ; 

// Bien 
var  fullName  =  'Bob'  +  this.lastName ;
```

- Si se utiliza en gran cantidad, las cadenas largas. Con concatenación afecta el rendimiento.
```javascript
// Mal
var errorMessage = 'This is a super long error that was thrown because of Batman. When you stop to think about how Batman had anything to do with this, you would get nowhere fast.';

// Mal
var errorMessage = 'This is a super long error that \
was thrown because of Batman. \
When you stop to think about \
how Batman had anything to do \
with this, you would get nowhere \
fast.';

// Bien
var errorMessage = 'This is a super long error that ' +
  'was thrown because of Batman.' +
  'When you stop to think about ' +
  'how Batman had anything to do ' +
  'with this, you would get nowhere ' +
  'fast.';
```

- Cuando se contruye una cadena mediante programación, utilice Array#join.

```javascript
var items,
    messages,
    length, i;

messages = [{
    state: 'success',
    message: 'This one worked.'
},{
    state: 'success',
    message: 'This one worked as well.'
},{
    state: 'error',
    message: 'This one did not work.'
}];

length = messages.length;

// Mal
function inbox(messages) {
  items = '<ul>';

  for (i = 0; i < length; i++) {
    items += '<li>' + messages[i].message + '</li>';
  }

  return items + '</ul>';
}

// Bien
function inbox(messages) {
  items = [];

  for (i = 0; i < length; i++) {
    items[i] = messages[i].message;
  }

  return '<ul><li>' + items.join('</li><li>') + '</li></ul>';
}
```

## Funciones

- Declarar funciones :

```javscript
// Definición una función anonima
var anonymous = function() {
  return true;
};

// Definición función 
var named = function named() {
  return true;
};

// Funciones de inmediata invocacion
(function() {
  console.log('Welcome to the Internet. Please follow me.');
})();
```

- Nunca declarar una función en un ámbito que no es una función (`if`, `while`, etc). En su lugar, asigne la función a una variable. Los navegadores le permiten hacer esto, pero la interpretación de que no es legal. De esta manera usted puede tener una mala noticia en cualquier momento.

- Nunca nombre un parámetro como `arguments` . Esto anula el objeto `arguments` que se pasan a cada función.

```javscript
// Mal
function nope(name, options, arguments) {
  // ...stuff...
}

// Bien
function yup(name, options, args) {
  // ...stuff...
}
```

## Propiedades

- Use punto `.` para acceder a las propiedades

```javascript
var luke = {
  jedi: true,
  age: 28
};

// Mal
var isJedi = luke['jedi'];

// Bien
var isJedi = luke.jedi;
```

- Use corchetes `[]` para acceder a las propiedades atravéz de una variable

```javascript
var luke = {
  jedi: true,
  age: 28
};

function getProp(prop) {
  return luke[prop];
}

var isJedi = getProp('jedi');
```

## Variable

- Utilice siempre `var` para declarar variables

```javascript
// Mal
superPower = new SuperPower();

// Bien
var superPower = new SuperPower();
```

- Use un `var` para hacer declaraciones multiples, deben hacerse de a una fila.

```javascript
// Mal
var items = getItems();
var goSportsTeam = true;
var dragonball = 'z';

// Mal
var i, items = getItems(),
    dragonball,
    goSportsTeam = true,
    len;

// Bien
var items = getItems(),
    goSportsTeam = true,
    dragonball = 'z';

// Bien
var items = getItems(),
    goSportsTeam = true,
    dragonball,
    length,
    i;
```

## Expresiones condicionales y la Igualdad

- Utilice `===` y `!==` o `==` y `!=`.

- Expresiones condicionales evaluando si es `Bollean` y es de una sola expresión:

  + Objects evaluación a true
  + Undefined evaluación a false
  + Null evaluación a false
  + Bolleans evaluación a el tipo de valor del bollean
  + Numbers evaluación a false, si +o, -o o NaN es true
  + String evaluación a false, si es un string vacio `''` es true

```javascript
if ([0]) {
  // true
}
```

- Utilizar metodos abreviados.

```javascript
// Mal
if (name !== '') {
  // ...stuff...
}

// Bien
if (name) {
  // ...stuff...
}

// Mal
if (collection.length > 0) {
  // ...stuff...
}

// Bien
if (collection.length) {
  // ...stuff...
}
```
## Bloques

- Utilizar llaves en todo los bloques cuando en va en varias lineas.


```javascript
// Mal
if (test)
  return false;

// Bien
if (test) return false;

// Bien
if (test) {
  return false;
}

// Mal
function() { return false; }

// Bien
function() {
  return false;
}
```

## Comas

- Comas al principio

```javascript
// Mal
var once
  , upon
  , aTime;

// Bien
var once,
    upon,
    aTime;

// Mal
var hero = {
    firstName: 'Bob'
  , lastName: 'Parr'
  , heroName: 'Mr. Incredible'
  , superPower: 'strength'
};

// Bien
var hero = {
  firstName: 'Bob',
  lastName: 'Parr',
  heroName: 'Mr. Incredible',
  superPower: 'strength'
};
```

## Conversiones

- String

```javascript
//  => this.reviewScore = 9;

// Mal
var totalScore = this.reviewScore + '';

// Bien
var totalScore = '' + this.reviewScore;

// Mal
var totalScore = '' + this.reviewScore + ' total score';

// Bien
var totalScore = this.reviewScore + ' total score';
```

- Utilizar `parseInt` para numeros

```javascript
var inputValue = '4';

// Mal
var val = new Number(inputValue);

// Mal
var val = +inputValue;

// Mal
var val = inputValue >> 0;

// Mal
var val = parseInt(inputValue);

// Bien
var val = Number(inputValue);

// Bien
var val = parseInt(inputValue, 10);
```

- Boolenas

```javascript
var age = 0;

// Mal
var hasAge = new Boolean(age);

// Bien
var hasAge = Boolean(age);

// Bien
var hasAge = !!age;
```

# Convenciones de nomenclatura

- Evite los nombres de una sola letra. Sea descriptivo con su nombramiento.

```javascript
// Mal
function q() {
  // ...stuff...
}

// Bien
function query() {
  // ..stuff..
}
```

- Utilize camelCase en nombre de objectos, functions y instancias

```javascript
// Mal
var OBJEcttsssss = {};
var this_is_my_object = {};
var this-is-my-object = {};
function c() {};
var u = new user({
  name: 'Bob Parr'
});

// Bien
var thisIsMyObject = {};
function thisIsMyFunction() {};
var user = new User({
  name: 'Bob Parr'
});
```

- Utilize PascalCase para nombramiento de contructores o clases

```javascript
// Mal
function user(options) {
  this.name = options.name;
}

var bad = new user({
  name: 'nope'
});

// Bien
function User(options) {
  this.name = options.name;
}

var good = new User({
  name: 'yup'
});
```

- Utilize  underscore _ para nombrar propiedades privadas

```javascript
// Mal
this.__firstName__ = 'Panda';
this.firstName_ = 'Panda';

// Bien
this._firstName = 'Panda';
```

- Nombre de funciones. al asignarse a una variable

```javascript
// Mal
var log = function(msg) {
  console.log(msg);
};

// Bien
var log = function log(msg) {
  console.log(msg);
};
```


[palabras reservadas]:https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Reserved_Words?redirectlocale=en-US&redirectslug=JavaScript%2FReference%2FReserved_Words
