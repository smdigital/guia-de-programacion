![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de proyectos FLASH
===========================

Este documento comprende lo que debe considerar las normas y considreaciones que se requieren para garantizar un alto nivel ténico en los proyectos que se desarrollen con FLASH.

# Tabla de contenido
1. [Organización del proyecto](#markdown-header-organizacion-del-proyecto)
2. [Document Class](#markdown-header-document-class)
3. [Configuración del escenario](#markdown-header-configuracion-del-escenario)
4. [Organización de la librería](#markdown-header-organizacion-de-la-libreria)
    1. [Fuentes](#markdown-header-fuentes)
5. [Recomendaciones generales](#markdown-header-recomendaciones-generales)
6. [Recursos externos](#markdown-header-recursos-externos)


## Organización del proyecto

Es importante crear 3 carpetas con nombres:

![Estructura de carpetas](http://www.smdigitalsites1.com/images_as3_guide/carpetas.png "estructura de carpetas")

**fla**

Conformada por todos los archivos AS y Flash que conforman el proyecto. 

Es importante crear un **loader** para cada proyecto, este es principalmente util para cargar la pelicula principal y poder mostrarle algun tipo de información al usuario mientras es cargada. Este archivo debe ser lo más liviano posible para evitar que el usuario se canse o cierre la aplicación. Normalmente el nombre seria nombre_proyectoLoader.fla.

Por ejemplo:

Si el proyecto se llama `Chocomundo`, el nombre del loader seria `ChocomundoLoader.fla`.


![Proyecto FlashDevelop](http://www.smdigitalsites1.com/images_as3_guide/estructura_flashdeveloper.png "proyecto Flash Develop")

En la foto anterior se ve como seria la estructura de archivos y carpetas. Este esquema se hizo con el software FlashDevelop que es uno de los mejores a la hora codificar para ActionScript ya que tiene una gran cantidad de herramientas como snippets, templates, entre otras.

En la carpeta `classes` deben estar todos los paquetes(carpetas) especificos del proyecto y estos pueden variar dependiendo el tipo de proyecto que se este realizando. Como se ve con el paquete `display` destinado para las clases que manejan componentes de visualización, pero es posible que tambien pueda existir una carpeta por ejemplo `net` para clases que manejan conexiones a bases de datos.

Es altamente recomendable como se ve en la imagen mantener los FLA con el mismo nombre que la clase que lo Controla.

Para el resto de clases que sean de uso general como frameworks, SWCs, o paquetes deben ir en el root del proyecto (o tambien es posible que esten en una dirección global para reutilizar en otros proyectos sin necesidad de duplicarlos), es decir al mismo nivel de las carpetas classes y assets.

En la carpeta `assets` se guardan todas las imagenes que se van a usar dentro del proyecto Flash como imagenes, iconos, fondos, etc.

![Carpeta de Assets](http://www.smdigitalsites1.com/images_as3_guide/assets.png "carpeta assets")

**www**

Contiene los archivos publicos como imagenes externas, swfs, xml, js, html, etc. Esta es la unica carpeta que debe ser subida al servidor una vez en producción.

Dependiendo del tipo de proyecto esta carpeta puede tener una esctructura diferente, si es un proyecto hibrido es posible que algunos archivos como librerias de js(swfobject, flashscale) esten por fuera y organizadas según el Framework sobre el que se este trabajando.

![Carpeta WWW](http://www.smdigitalsites1.com/images_as3_guide/www.png "carpeta www")

**resources**

En esta carpeta se deben colocar todos los archivos necesarios para la realización del proyecto como PSDs, PDFs, Docs, etc. 

![Carpeta de recursos](http://www.smdigitalsites1.com/images_as3_guide/resources.png "carpeta recursos")

## Document Class

Siempre que iniciemos un proyecto Flash debemos crear una clase principal que se encargue de controlar el SWF principal, debemos evitar a toda cosa escribir codigo en la linea de tiempo del archivo FLA, esto es una mala practica por mucho motivos.

Esctrutura basica:
```as3
package classes 
{
    // importe de clases principales
    import flash.display.MovieClip;
    
    // Esta clase siempre debe extender de MovieClip
    public class App extends MovieClip 
    {
        //definición de variables
        private var _vars:Object; 
        
        public function App() 
        {
            super();
            
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);
        }
        
        private function addedToStageHandler(event:Event):void 
        {
            removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
            
            //configuramos las propiedades del stage
            stage.align = "topLeft";
            stage.scaleMode = "noScale";
            stage.showDefaultContextMenu = false;
            stage.stageFocusRect = false;

            //en caso que sea reescalable
            stage.addEventListener(Event.RESIZE, resize);

            addEventListener(Event.ENTER_FRAME, enterFrameHandler);
        }
        
        private function enterFrameHandler(event:Event):void 
        {
            if (stage.stageWidth > 0 && stage.stageHeight > 0)
            {
                removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
                
                initApp();
            }
        }
        
        private function initApp():void
        {
            //creamos los diferentes objetos que dan inicio a la app.
            //cargar XMLs, imagenes, etc

            trace("hola mundo");
        }

        //En caso que la aplicación sea reescalable
        private function resize(event:Event = null):void
        {
            //tomamos el tamaño del stage
            var w:uint = stage.stageWidth;
            var h:uint = stage.stageHeight;

            //reorganizamos el escenario según sea el caso
        }              
    }
}
```

Ahora simplemente asignamos en el archivo FLA la clase controladora.

![Document Class](http://www.smdigitalsites1.com/images_as3_guide/documentclass.png "document class")

## Configuración del escenario

Normalmente una pelicula debe ser configurada a 30 FPS. Esta cantidad es suficiente para que una pelucula se vea bien fluida a la vez que trata bien el equipo del usuario. Entre más FPS más consumo de procesador y recursos.

El tamaño de una pelicula varia mucho, dependiendo el tipo de proyecto el tamaño puede ser fijo o variable. Aqui alguno de los tamaños más utilizados:

Para un TAB de Facebook  **Width:810px - Height:900px**

Para un sitio web en Flash **Width: 960px - Height:610px**

![Configuración del escenario](http://www.smdigitalsites1.com/images_as3_guide/configuracion.png "configuracion escenario")

### Organización de la librería

Mantener organizada la librería del proyecto es esencial. Esto evitará posteriores dolores de cabeza con elementos duplicados y hará más facíl la ubicación de los mismos. 

Es recomendables organizarlos por componentes como se muestra en la figura:

![Librería](http://www.smdigitalsites1.com/images_as3_guide/libreria.png "libreria")

### Fuentes

Se debe crear en la librería una carpeta destinada a estas. Es muy recomendable no usar muchas variaciones de fuentes ya que pueden subir mucho el peso del SWF. Se pueden usar entre 2 o 3 maximo siempre teniendo en cuenta el peso de la pelicula. No olvidar hacer **embed** siempre antes de utilizarlas, esto permite la perfecta visualización en los diferentes browser y sistemas operativos.

![Manejo de fuentes](http://www.smdigitalsites1.com/images_as3_guide/fonts.png "fuentes")

## Recomendaciones generales

 - El peso promedio para una pelicula debe ser entre 100k y 400k. Esto dependerá de la complejidad del proyecto.
 - Siempre cargar las imagenes que no hagan parte de la UI como recursos externos.
 - Siempre crear un preloader para la pelicula principal.

## Recursos externos

[Goto and Learn](http://www.gotoandlearn.com/) - Videotutoriales para aprender Flash

[TweenMax](http://www.greensock.com/tweenmax/) - Motor de animación

[AS3 101](http://dev.tutsplus.com/series/as3-101--active-7395) - Introducción a AS3

[Flash Develop](http://www.flashdevelop.org/) - Editor de Codigo libre para AS3



