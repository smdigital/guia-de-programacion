![SMDigital logo](http://www.smdigital.com.co/wp-content/themes/sm-digital/images/sm-digital-logo.png "SMDigital Logo")

Guía de programación AS3
===========================

Este documento comprende lo que debe considerar las normas de codificación que se requieren para garantizar un alto nivel ténico de interoperabilidad entre el código AS3 y Flash.

# Tabla de contenido
1. [Encoding](#markdown-header-enconding)
2. [Operadores](#markdown-header-operadores)
3. [Declaraciones](#markdown-header-declaraciones)
4. [Valores Hexadecimales](#markdown-header-valores-hexadecimales)
5. [Cadenas literales](#markdown-header-cadenas-literales)
6. [Vectores y Objetos](#markdown-header-vectores-y-objetos)
7. [Expresiones regulares literales y XMLs ó XMLList](#markdown-header-expresiones-regulares-literales-y-xmls-xmllist)
8. [Clases](#markdown-header-clases)
    1. [Declaración de variables y propiedades](#markdown-header-declaracion-de-variables-y-propiedades)
    2. [Constantes](#markdown-header-constantes)
    3. [Métodos de clase](#markdown-header-metodos-de-clase)
    4. [Getters y Setters](#markdown-header-getters-y-setters)
    5. [Eventos](#markdown-header-eventos)
    6. [Manejadores de Eventos (Event Handlers)](#markdown-header-manejadores-de-eventos-event-handlers)
9. [Estructuras de control](#markdown-header-estructuras-de-control)
10. [Comentarios](#markdown-header-comentarios)

## Enconding

Todos los archivos AS3 deben estar codificados UTF-8

## Operadores

Todos los operadores binarios, tales como `+, -, =, !=, ==, >,` entre otros, deben tener un espacio antes y despues del operador para mejorar la lectura.

Por ejemplo:
```as3
//utilizalo de esta forma
var foo:int = a + b;

//no esto
var foo:int=a+b;
```

Para operadores que trabajan sobre un unico valor, tales como `++, --, +=` no debe haber espacio entre el operador y la variable.

Por ejemplo:
```as3
var a:uint = 3;
var b:uint = 5;
b+=a;
```

## Declaraciones

Siempre termine las lineas de código con un `;`;

Por ejemplo:
```as3
//Utiliza esta forma
var a:uint = 3;
var b:uint = 5;

//No esta
var a:String = 'lorem ipsum'
var b:Number = Math.round() + 5
```

## Valores Hexadecimales

Utiliza la `x` en minuscula y el número hexadecimal en mayuscula. Recuerda que los números hexadecimales tienen 6 digitos hexadecimales.
Por ejemplo:
```as3
//Utiliza esta forma
var color:uint = 0xFF0000;

//No esta
var color:uint = 0xff0000;

//utiliza esta forma
var black:uint = 0x000000;

//no esta 
var black:uint = 0;
```

## Cadenas literales

Utiliza siempre las dobles comillas y no las simples para delimitar las cadenas, aún si la cadena contiene comillas como caracter.
Por ejemplo:
```as3
//Utiliza esta forma
var message:String = "lorem \"ipsum\"";

//No esta
var message:String = 'lorem "ipsum"';
```

## Vectores y Objetos

Utiliza vecotres literales en vez de `new Array()` y lo mismo para los objetos.
Por ejemplo:
```as3
//Utiliza esta forma
var numbers:Array = [5,4,8,9,6,54,4];
var user:Object = {name:"lorem", lastName:"Ipsum", id:683218431};

//No esta
var numbers:Array = new Array(5,4,3,2);
var user:Object = new Object();
user.name = "Lorem";
user.lastName = "Ipsum";
user.id = 683218431;
```

## Expresiones regulares literales y XMLs ó XMLList

Utiliza la notación literal en vez de crear una instancia desde una cadena.
Por ejemplo:
```as3
//Utiliza esta forma
var pattern:RegExp = /\d+/g;

var node:XML = <content>
                    <item id="163543" src="images/demo.png" />
                    <item id="163543" src="images/demo1.png" />
                    <item id="163543" src="images/demo2.png" />
                </content>;

//No esta
var pattern:RegExp = new RegExp("\\d+", "g");

var node:XML = new XML("<content><item id=\"745416131\" src=\"images/demo.png\"  />");
```

## Clases

Las clases siempre deben tener un paquete especifico, el nombre debe comenzar con mayuscula: `Slider` , `SliderItem`, etc.

Por ejemplo:
```as3
package classes.display  //paquete que contiene la clase
{
    //Definición de la clase
    public class FooClass
    {
        //constructor
        public function FooClass():void
        {

        }
    }
}
```

### Declaración de variables y propiedades

Las propiedades de la clase se deben comenzar  por `_` seguido de la primera letra en minuscula o combiando dos palabras con la segunda comanzando en Mayuscula. Todas deben tener especificado su nivel de acceso definido por `public`, `private`, `protected` o `internal` y que tipo de variable es.

Para las propiedades públicas no es necesario colocar el guión bajo al inicio de la declaración.

Por ejemplo:

```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        private var _items:Array;
        private var _label:String;
        protected var _assets:MovieClip;
        public var distance:uint;

        public function DemoClass():void
        {

        }
    }
}
```

### Constantes

Este tipo de propiedades deben estar totalmente en mayuscula y en caso de que sean varias palabras deben ser unidas pod guión bajo.

Por ejemplo:
```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        // constants
        public static const VERSION:String ="1.1";
        public static const NAME_BUTTON:String = "Send";

        private var _items:Array;
        
        public function DemoClass():void
        {

        }
    }
}
```

### Métodos de clase

Deben estar definidos por la palabra reservada `public`, `protected`, `private` segun el nivel de acceso que se le quiera dar al metodo. El nombre debe comenzar por minuscula y en caso de que sea compuesto con mayuscula inicial en la segunda. Estos siempre deben ser verbos.

Por ejemplo:
```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        private var _items:Array;
        
        public function DemoClass():void
        {

        }

        private function createItems():void
        {
            ...
        }

        public function hide():void
        {
            ...
        }

        protected function checkPosition():void
        {
            ...
        }
    }
}
```

Escoger buenos nombres para las variables, funciones y clases es critico para crear codigo que es facil de usar y entender.

#### Abreviaturas

Evitalas como regla general. Por ejemplo, `calcularValorTotal()` es mejor nombre que `calValT()`.

#### Acronimos

Deben estar siempre en mayusculas. 

```as3
//Utiliza esta forma
var imageURL:String = "";

//No esta
var imageUrl:String = "";
```

### Getters y Setters

Deben estar definidos por la palabra reservada `public`. El nombre debe tener el mismo nombre de la propiedad al que esta asociada y en caso de que sea compuesto con mayuscula inicial en la segunda. En el caso de un Setter se debe especificar `void` como valor de retorno y para los Getter el valor que corresponda. Evitar `*` como valor de retorno.

Por ejemplo:
```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        private var _items:Array;
        
        public function DemoClass():void
        {

        }

        //Utiliza esta forma
        public function set items(value:Array):void
        {
            _items = value;
        }

        //no esta
        public function set items(value)
        {
            _items = value;
        }

        //Utiliza esta forma
        public function get items():Array
        {
            return _items;
        }

        //No esta
        public function get items():*
        {
            return _items;
        }
    }
}
```

### Eventos

Al ser constantes, estas utilizan las mismas normas. En caso de que sea una palabra compuesta debe ser separada por `_`.

Por ejemplo:
```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        //Utiliza esta forma
        public static const BUTTON_CLICK:String = "buttonClick";

        //no esta
        public static const BUTTONCLICK:String = "buttonClick";
        
    }
}
```

### Manejadores de Eventos (Event Handlers)

Los manejadores de eventos son métodos un poco diferentes a un método normal. Siempre reciben un evento especifico, en algunos casos es posible que este sea es **null**. Estos se deben nombrar concatenando la palabra `Handler` al nombre del método. Se recomienda al crear el manejador de eventos que éste sea precedido por la palabra `private` o `protected` pero no `public`.

Por ejemplo:
```as3
package classes.display
{
    public class DemoClass extends MovieClip
    {
        
        public function DemoClass():void
        {
            //utiliza esta forma
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);

            //o esta
            addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler, false, 0, true);

            //No esta
            this.addEventListener(Event.ADDED_TO_STAGE, addedToStage);
        }

        private function addedToStageHandler(event:Event):void
        {

        }
        
    }
}
```

En caso de que el manejador se quiera ejecutar sin necesidad del evento, se especifica el argumento como null.

```as3
private function addedToStageHandler(event:Event = null):void
{

}
```

## Estructuras de control

Estas incluyen `if, for, while, switch`, etc. Aqui un ejemplo:

```as3
if(condition1 || condition2) 
{
    action1;
}
elseif(condition3 && condition4) 
{
    action2;
}
else 
{
    defaultaction;
}
```

Sin espacio entre el nombre de la estructura de control y el parentesis que abre. Siempre se debe usar las llaves aun en situaciones donde sean opcionales. Agregandolos se hace más facil la lectura y se minimiza los errores logicos cuando se agregan nuevas lineas.

para un **switch**, sigue el siguiente ejemplo:

```as3
//Utiliza esta forma
switch (condition) 
{
    case 1:
    {
        action1;
    }  
    break;

    case 2:
    {
        action2;
    }
    break;

    default:
    {
        defaultaction;
    }

    break;
}

//No esta forma
switch (condition) 
{
    case 1:
        action1;
    break;

    case 2:
    {
        action2;
    }
    break;

    default:
        defaultaction;
    break;
}
```

## Comentarios

Los comentarios deben ser creados usando la forma que de los comentarios de ASDoc

```as3
/** Lorem ipsum ucpela  */
```

Notese que comienza con 2(*).

Si el comentario tiene 2 ó mas lineas, debe ser escrito con un (*) al comienzo de una nueva linea.

```as3
/** 
* Comentario...
* el comentario continua...
*/
```